package co.java.corona.tracker.model;

public class Stats {

	private String state;
	private String country;
	private int latestTotalReportedCases;
	private int difference;

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getLatestTotalReportedCases() {
		return latestTotalReportedCases;
	}
	public void setLatestTotalReportedCases(int latestTotalReportedCases) {
		this.latestTotalReportedCases = latestTotalReportedCases;
	}
	public int getDifference() {
		return difference;
	}
	public void setDifference(int difference) {
		this.difference = difference;
	}
}

package co.java.corona.tracker.controller;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.java.corona.tracker.model.Stats;
import co.java.corona.tracker.services.DataService;

@Controller
public class HomeController {

	@Autowired
	DataService dataService;

	@GetMapping("/home")
	public String home(Model model) {
		List<Stats> allData = dataService.getAllData().stream()
				.sorted(Comparator.comparingInt(Stats::getLatestTotalReportedCases).reversed())
				.collect(Collectors.toList());
		List<Stats> allNewData = dataService.getAllData().stream()
				.sorted(Comparator.comparingInt(Stats::getDifference).reversed())
				.collect(Collectors.toList());
		model.addAttribute("stats", allNewData);
		model.addAttribute("totalCases", 
				allData.stream().mapToInt(stat -> stat.getLatestTotalReportedCases()).sum());
		model.addAttribute("newTotal", 
				allData.stream().mapToInt(stat -> stat.getDifference()).sum());
		return "home";
	}
}

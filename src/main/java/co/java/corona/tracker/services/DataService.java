package co.java.corona.tracker.services;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import co.java.corona.tracker.model.Stats;

@Service
public class DataService {

	private final RestTemplate restTemplate;
	
	public DataService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = new RestTemplate(new BufferingClientHttpRequestFactory(new HttpComponentsClientHttpRequestFactory()));
	}

	@Value("${virus.data.url}")
	private String virusDataUrl;

	private List<Stats> allData = new ArrayList<>();

	public List<Stats> getAllData() {
		return allData;
	}

	@PostConstruct
	@Scheduled(cron = "* * 1 * * *")
	public void fetchData() throws IOException, InterruptedException {

		ResponseEntity<String> response = restTemplate.exchange(virusDataUrl, HttpMethod.GET, null, String.class);
		StringReader csvBodyReader = new StringReader(response.getBody());

		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(csvBodyReader);
		List<Stats> newData = new ArrayList<>();
		Stats stat = null;
		for (CSVRecord record : records) {
			stat = new Stats();
		    stat.setState(record.get("Province/State"));
		    stat.setCountry(record.get("Country/Region"));
		    int latestCases = Integer.parseInt(record.get(record.size()-1));
		    int previousDayCases = Integer.parseInt(record.get(record.size()-2));
			stat.setLatestTotalReportedCases(latestCases);
			stat.setDifference(latestCases - previousDayCases);
		    newData.add(stat);
		}
		this.allData = newData;
	}

}
